psychtoolbox-3 (3.0.9+svn2579.dfsg1-2) UNRELEASED; urgency=low

  * Added recommended by Mario (upstream) tools for troubleshooting
    (intel-gpu-tools, apitrace, powertop, latencytop) into Suggests of
    the -lib package

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 03 Oct 2012 10:01:15 -0400

psychtoolbox-3 (3.0.9+svn2579.dfsg1-1) unstable; urgency=low

  * Fresh always-beta always 3.0.9 release
    - addresses portaudio issues on amd64 platform
    - 1 new entry for debian/copyright

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 23 May 2012 13:25:28 -0400

psychtoolbox-3 (3.0.9+svn2539.dfsg1-1) unstable; urgency=low

  * Fresh beta release:
    - resolved outstanding copyright/license issue with glext_edit.h
    - Initial upload to unstable (Closes: #606557)
  * debian/control:
    - -common: recommend subversion for DownloadAdditions functionality
  * debian/copyright:
    - updated to reflect upstream changes and some outstanding missing entries
  * debian/TODO, debian/rules:
    - cleanup

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 02 Apr 2012 11:12:15 -0400

psychtoolbox-3 (3.0.9+svn2514.dfsg1-1) neurodebian; urgency=low

  * Fresh beta release:
    Many improvements to realtime scheduling and movie playback, a bit of
    gpu based random number noise generation (perlin noise) on modern
    gpus, etc.
  * Refreshed patches
  * Updated copyright (years, URLs, license)
  * Boosted policy to 3.9.3 -- no changes
  * Added upstream changelog as a GIT/SVN log of the upstream beta branch
    which carries release summaries
  * "Manually" generate octave:Depends due to deprecation of
    octave-depends helper in favor of unversioned octave package.

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 22 Mar 2012 22:19:03 -0400

psychtoolbox-3 (3.0.9+svn2458.dfsg1-1) neurodebian; urgency=low

  * New upstream snapshot (no chages in sources), necessary
    to assure correct fetching of binary extensions upon demand

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 19 Feb 2012 21:00:54 -0500

psychtoolbox-3 (3.0.9+svn2456.dfsg1-1) neurodebian; urgency=low

  * New upstream snapshot (no chages in sources), necessary
    to assure correct fetching of binary extensions upon demand
  * To please Ubuntu's --as-needed placed -l's for
    libptbdrawtext_ftgl.so.1 build at the end of the build cmd
    (should fix incorrect building on Ubuntu's >= 11.04)

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 17 Feb 2012 21:59:12 -0500

psychtoolbox-3 (3.0.9+svn2455.dfsg1-1) neurodebian; urgency=low

  * New upstream snapshot
    - upstream dropped direct build dependency on libraw1394-dev
    - compatible with freenect 0.1.2
  * Refreshed patches: deb_no_static_bindings_with-fPIC,
    deb_see_README.Debian, deb_use_system_GLEW

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 12 Feb 2012 18:29:24 -0500

psychtoolbox-3 (3.0.9+svn2380.dfsg1-1) neurodebian; urgency=low

  * New upstream snapshot addressing XRandR support etc
  * Upstream moved to a new SVN hosting, git-svn linkage was updated
  * deb_see_README.Debian patch refreshed
  * Boosted policy to 3.9.2 -- no changes

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 01 Dec 2011 00:19:52 -0500

psychtoolbox-3 (3.0.9+svn2351.dfsg1-1) neurodebian; urgency=low

  * New upstream point release
  * Adjusted debian/cleanup.sh (thanks Mario for heads up)
  * Renamed debian/cleanup.sh into debian/dfsg-upstream
  * libxrandr-dev into Build-Depends

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 07 Nov 2011 22:02:20 -0500

psychtoolbox-3 (3.0.9+svn2265.dfsg1-1) neurodebian; urgency=low

  * New upstream point release
  * gfortran into build-depends to assure noise-less operation of
    mkoctfile
  * libxi-dev into Build-Depends due to use of libXi
  * Enabled building of moalcore.mex and PsychHID.mex for octave
    (libopenal-dev into build-Depends)
  * Build-Depend on 1.6 (instead of 1.5) version of GLEW to enable
    OML_sync_control extension

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 09 Aug 2011 14:25:00 -0400

psychtoolbox-3 (3.0.9+svn2167.dfsg1-1) neurodebian; urgency=low

  * New upstream point release (aiming at becoming a beta):
    - core is now distributed under MIT license
  * Removed BIG FAT WARNING
  * Added Recommends and Suggests on some octave toolboxes, and
    gstreamer plugins sets (base, good, bad, ugly)
  * Provide include paths for glib-2.0 from pkg-config -- necessary
    for build on systems with multiarch support

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 27 Jun 2011 22:55:34 -0400

psychtoolbox-3 (3.0.9+svn2078.dfsg1-1) neurodebian; urgency=low

  * New upstream point release

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 17 May 2011 18:00:28 -0400

psychtoolbox-3 (3.0.8+svn1934.dfsg1-1~pre2) neurodebian; urgency=low

  * New patch up_portaudio_patches to consolidate upstream's
    linux-relevant patches for portaudio -- fixes problem with missing
    symbol PaUtil_SetDebugPrintFunction
  * README.Debian:
    - notes on setting up real-time scheduling and memory locking
    - hint on 'sca' function for restoring the display
  * New patch deb_psychtoolboxversion to enable reporting PTB version
    in the packaged version of PTB
  * Added a big fat warning to descriptions of all the packages

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 20 Jan 2011 12:17:42 -0500

psychtoolbox-3 (3.0.8+svn1934.dfsg1-1~pre1) neurodebian; urgency=low

  * Initial Debian packaging

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 17 Dec 2010 00:01:44 -0500
